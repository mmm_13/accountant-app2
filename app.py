import copy

from flask import Flask, render_template, request
from flask_sqlalchemy import SQLAlchemy
from accountant import mg, SavePrzeglad
from flask_migrate import Migrate

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///accountant2.db"
db = SQLAlchemy(app)
migrate = Migrate(app, db, render_as_batch=True)


class Products(db.Model):
    name = db.Column(db.String(50), primary_key=True)
    amount = db.Column(db.Integer, nullable=False, unique=False)


class Actions(db.Model):
    pk = db.Column(db.Integer, primary_key=True)
    action = db.Column(db.String(15), nullable=False, unique=False)
    quota = db.Column(db.Integer, nullable=True, unique=False)
    comment = db.Column(db.String(40), nullable=True, unique=False)
    product = db.Column(db.String(40), nullable=True, unique=False)
    price = db.Column(db.Integer, nullable=True, unique=False)
    amount = db.Column(db.Integer, nullable=True, unique=False)
    balance = db.Column(db.Integer, nullable=False, unique=False)


db.create_all()


# for element in mg.actions:
#     action = element[0]
#     params = element[1]
#     if action in mg.callbacks:
#         mg.callbacks[action](params)
#         if action == 'saldo':
#             actions_input = Actions(action=action, quota=params[0], comment=params[1], balance=mg.balance)
#         else:
#             actions_input = Actions(action=action, product=params[0], price=params[1], amount=params[2],
#                                     balance=mg.balance)
#             test_product = db.session.query(Products).filter(Products.name==params[0]).first()
#             if not test_product:
#                 products_input = Products(name=params[0], amount=params[2])
#                 db.session.add(products_input)
#             else:
#                 if action == 'zakup':
#                     test_product.amount += int(params[2])
#                 else:
#                     test_product.amount -= int(params[2])
#         db.session.add(actions_input)
#     db.session.commit()


def update_manager():
    actions = db.session.query(Actions).all()
    for action in actions:
        if action.action == "saldo":
            params = [action.quota, action.comment]
        else:
            params = [action.product, action.price, action.amount]
        mg.actions.append((action.action, params))


def update_main_page():
    balance = mg.balance
    products = mg.products
    products_list = []
    for product in products:
        element = products[product]
        products_list.append((element.name, element.amount))
    return balance, products_list


def modify_results(results_list):
    new_list = []
    while results_list:
        temp_list = []
        for i in range(2):
            temp_list.append((results_list.pop(0)))
        new_list.append(temp_list)
    return new_list


templates_dict = {'saldo': 'saldo.html', 'zakup': 'zakup.html', 'sprzedaz': 'sprzedaz.html'}

update_manager()
mg.execute()


@app.route('/')
def start_app():
    balance, products_list = update_main_page()
    return render_template('main_page.html', balance=balance, products=products_list)


@app.route('/main/')
def main_page():
    balance, products_list = update_main_page()
    return render_template('main_page.html', balance=balance, products=products_list)


@app.route('/saldo/')
def saldo_page():
    return render_template('saldo.html')


@app.route('/sprzedaz/')
def sprzedaz_page():
    return render_template('sprzedaz.html')


@app.route('/zakup/')
def zakup_page():
    return render_template('zakup.html')


@app.route('/historia/')
def historia_page():
    saver = SavePrzeglad()
    results = saver.make_results(1, len(mg.actions))
    results = modify_results(results)
    return render_template('historia.html', results=results)


@app.route('/historia/resp', methods=['post'])
def historia_resp_page():
    resp_dict = dict(request.form)
    first_idx = resp_dict["first_idx"]
    second_idx = resp_dict["second_idx"]
    saver = SavePrzeglad()
    results = saver.make_results(first_idx, second_idx)
    results = modify_results(results)
    return render_template('historia.html', results=results)


@app.route('/historia/<first_idx>/<second_idx>/')
def historia_page_idx(first_idx, second_idx):
    saver = SavePrzeglad()
    results = saver.make_results(first_idx, second_idx)
    results = modify_results(results)
    return render_template('historia.html', results=results)


@app.route('/resp/', methods=['post'])
def resp_page():
    resp_dict = dict(request.form)
    params = []
    for i in resp_dict.values():
        params.append(i)
    action = params[0]
    action_results = mg.callbacks[action](params[1:])
    if action_results == 'OK':
        mg.temporary_actions = copy.copy(params)
        mg.update_store()
        balance, products_list = update_main_page()
        if action == 'saldo':
            actions_input = Actions(action=action, quota=params[1], comment=params[2], balance=mg.balance)
        else:
            actions_input = Actions(action=action, product=params[1], price=params[2], amount=params[3],
                                    balance=mg.balance)

            test_product = db.session.query(Products).filter(Products.name == params[1]).first()
            if not test_product:
                products_input = Products(name=params[1], amount=params[3])
                db.session.add(products_input)
            else:
                if action == 'zakup':
                    test_product.amount += int(params[3])
                else:
                    test_product.amount -= int(params[3])
        db.session.add(actions_input)
        db.session.commit()
        return render_template('main_page.html', balance=balance, products=products_list)
    else:
        template = templates_dict[action]
        return render_template(template, error=action_results)


if __name__ == '__main__':
    app.run(debug=True)
